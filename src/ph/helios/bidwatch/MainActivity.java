package ph.helios.bidwatch;

import ph.helios.bidwatch.API.WebAPI;
import ph.helios.bidwatch.API.WebAPIAsyntask;
import ph.helios.bidwatch.Model.Model_MerchantCategories;
import ph.helios.bidwatch.adapter.Adapter_MerchantCategory;
import ph.helios.bidwatch.adapter.Adapter_ProjectType;
import ph.helios.bidwatch.database.DbManager;
import ph.helios.bidwatch.listener.EventListener;
import ph.helios.bidwatch.utilities.CustomDialog;
import ph.helios.bidwatch.utilities.Utilities;
import ph.helios.bidwatch.utilities.Variables;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class MainActivity extends EventListener{
	public Animation ANIM_ZOOMIN;
	public Animation ANIM_ZOOMOUT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerIndicator= (LinearLayout) findViewById(R.id.drawerIndicator);
        layoutDrawer = (LinearLayout) findViewById(R.id.layoutDrawer);
        
    	ANIM_ZOOMIN 					= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
		ANIM_ZOOMOUT 					= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
	
         flipperHolder = (ViewFlipper) findViewById(R.id.flipperHolder);

         flipperHolder.setInAnimation(ANIM_ZOOMIN);
         flipperHolder.setOutAnimation(ANIM_ZOOMOUT);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
       
        getActionBar().setDisplayHomeAsUpEnabled(true);
        int titleId = getResources().getIdentifier("action_bar_title", "id","android");
        TextView yourTextView = (TextView) findViewById(titleId);
        yourTextView.setTypeface(Utilities.FontStyle( getAssets()));
        
        
        flipperHolder.addView(View.inflate(this, R.layout.activity_home, 								null), 0);
        flipperHolder.addView(View.inflate(this, R.layout.activity_opportunities, 						null), 1);
        flipperHolder.addView(View.inflate(this, R.layout.activity_budgetallocation, 						null), 2);
        flipperHolder.addView(View.inflate(this, R.layout.activity_supplier, 						null), 3);
    
         mTypeface = Typeface.createFromAsset(this.getAssets(), Utilities.STR_FONTS_ACCENGRALIG);

  
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 
                R.drawable.ic_navigation_drawer,   R.string.drawer_open, R.string.drawer_close ) { 
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
            }
        };
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
       
        listCategoryMerchant = (ListView)findViewById(R.id.listCategoryMerchant);
        listProcurementsList = (ListView)findViewById(R.id.listProcurementsList);
        listtxtProjectType = (ListView)findViewById(R.id.listtxtProjectType);
        listProcurementsListPerProject = (ListView)findViewById(R.id.listProcurementsListPerProject);
        listOrganizationHistory= (ListView)findViewById(R.id.listOrganizationHistory);
        txtOrganizationInfo = (TextView)findViewById(R.id.txtOrganizationInfo);
        flipperSupplier= (ViewFlipper)findViewById(R.id.flipperSupplier);

        flipperSupplier.setDisplayedChild(1);
        listProcurementsList.setVisibility(View.GONE);
       txtMerchantCategory = (EditText)findViewById(R.id.txtMerchantCategory);
       txtProjectType = (EditText)findViewById(R.id.txtProjectType);
       txtSupplier = (EditText)findViewById(R.id.txtSupplier);
		API 							= new WebAPI();
		dbm 							= new DbManager(this);
		

        ADAPTER_MERCHANTCATEGORIES	= new Adapter_MerchantCategory	    (getApplicationContext(), dbm.GetMerchantCategories(dbm,""),this);;
        ADAPTER_PROJECTTYPE	= new Adapter_ProjectType	    (getApplicationContext(), dbm.GetMerchantCategories(dbm,""),this);;
        							
        listCategoryMerchant.setAdapter(ADAPTER_MERCHANTCATEGORIES);
        listtxtProjectType.setAdapter(ADAPTER_PROJECTTYPE);
		if(!dbm.hasData(dbm, Model_MerchantCategories.TABLE)){
    	myAsyncTask = new WebAPIAsyntask(Variables.APIMODE_GETMERCHANTCATEGORY, API, getApplicationContext(),MainActivity.this);
		myAsyncTask.execute();
		}

  	  	CLASS_DIALOG   					= new CustomDialog();
		
		txtMerchantCategory.addTextChangedListener(new TextWatcher() {
	            @Override
	            public void onTextChanged(CharSequence s, int start, int before, int count) { }
	            @Override
	            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	            @Override
	            public void afterTextChanged(Editable s) {
	            	if(listCategoryMerchant.getVisibility() == View.VISIBLE)
	            		ADAPTER_MERCHANTCATEGORIES.updateAdapter(dbm.GetMerchantCategories(dbm, txtMerchantCategory.getText().toString()));
	            	else
	            		ADAPTER_PROCUREMENTLISTACTIVE.updateAdapter(dbm.GetProcurementListActive(dbm, txtMerchantCategory.getText().toString()));
	            }
	      });
		txtProjectType.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {
            	if(listtxtProjectType.getVisibility() == View.VISIBLE)
            		ADAPTER_PROJECTTYPE.updateAdapter(dbm.GetMerchantCategories(dbm, txtProjectType.getText().toString()));
            	else
            		ADAPTER_PROCUREMENTLISTACTIVE.updateAdapter(dbm.GetProcurementListActive(dbm, txtProjectType.getText().toString()));
            }
      });
    }
   
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
     
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
}
