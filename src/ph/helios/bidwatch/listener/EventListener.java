package ph.helios.bidwatch.listener;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ph.helios.bidwatch.R;
import ph.helios.bidwatch.API.WebAPIAsyntask;
import ph.helios.bidwatch.API.WebAPIAsyntask.APIListener;
import ph.helios.bidwatch.Model.Model_MerchantCategories;
import ph.helios.bidwatch.Model.Model_Organization;
import ph.helios.bidwatch.Model.Model_Procurement_List;
import ph.helios.bidwatch.adapter.Adapter_ProcurementListActive;
import ph.helios.bidwatch.adapter.Adapter_ProcurementListHistory;
import ph.helios.bidwatch.utilities.Variables;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class EventListener extends Variables implements APIListener, OnInterfaceClickListener{

	@Override
	public void onAPIStarted(int mode) {
		// TODO Auto-generated method stub
		dialog = ProgressDialog.show(EventListener.this, "", "Loading. Please wait...", true);
	}
	@Override
	public void onAPIFinished(String result, int mode, String... Params) {
		if(dialog.isShowing())
		dialog.dismiss();

		JSONObject OuterJSON,OuterJSON2;
		switch(mode){
		case Variables.APIMODE_GETMERCHANTCATEGORY:
			try {
				 OuterJSON = new JSONObject(result.replace("'", "*"));
				 OuterJSON2 = new JSONObject( OuterJSON.getString("result"));
					JSONArray user123 = new JSONArray(OuterJSON2.getString("records"));
					List<Model_MerchantCategories> list2 = new ArrayList<Model_MerchantCategories>();
					NumberFormat formatter = new DecimalFormat("#,##0.00");
					for(int i = 0; i < user123.length(); i++){
					  	JSONObject jsonobject = user123.getJSONObject(i);
						
						Model_MerchantCategories merchantCategories = new Model_MerchantCategories();
						merchantCategories.setTotolAllotedBudget(formatter.format(jsonobject.getDouble("sum")));
					    merchantCategories.setCategory(jsonobject.getString("business_category"));
					    
						list2.add(merchantCategories);
					}
					dbm.putData(dbm, Model_MerchantCategories.TABLE, list2, mode);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;
		case Variables.APIMODE_GETACTIVEPROCUREMENTBYCATEGORY:
			try {
				 OuterJSON = new JSONObject(result.replace("'", "*"));
				 OuterJSON2 = new JSONObject( OuterJSON.getString("result"));
					JSONArray user123 = new JSONArray(OuterJSON2.getString("records"));
					ArrayList<Model_Procurement_List> list2 = new ArrayList<Model_Procurement_List>();
					String category = "";
					for(int i = 0; i < user123.length(); i++){
					  	JSONObject jsonobject = user123.getJSONObject(i);
					  	category = jsonobject.getString("business_category");
					  	Model_Procurement_List procurementList = new Model_Procurement_List();
					  	procurementList.setBusinessCategory(jsonobject.getString("business_category"));
					  	procurementList.setClassification(jsonobject.getString("classification"));
					  	procurementList.setFunding_instrument(jsonobject.getString("funding_instrument"));
					  	procurementList.setCalendar_type(jsonobject.getString("calendar_type"));
					  	procurementList.setRef_id(jsonobject.getString("ref_id"));
					  	procurementList.setNotice_type(jsonobject.getString("notice_type"));
					  	procurementList.setFunding_source(jsonobject.getString("funding_source"));
					  	procurementList.setModified_date(jsonobject.getString("modified_date"));
					  	procurementList.setProcurement_mode(jsonobject.getString("procurement_mode"));
					  	procurementList.setCreated_by(jsonobject.getString("created_by"));
					  	procurementList.setContact_person_address(jsonobject.getString("contact_person_address"));
					  	procurementList.setApproved_budget(jsonobject.getString("approved_budget"));
					  	procurementList.setDescription(jsonobject.getString("description"));
					  	procurementList.setProcuring_entity_org(jsonobject.getString("procuring_entity_org"));
					  	procurementList.setCreation_date(jsonobject.getString("creation_date"));
					  	procurementList.setTrade_agreement(jsonobject.getString("trade_agreement"));
					  	procurementList.setTender_title(jsonobject.getString("tender_title"));
					  	procurementList.setClosing_date(jsonobject.getString("closing_date"));
					  	procurementList.setTender_status(jsonobject.getString("tender_status"));
					  	procurementList.setContract_duration(jsonobject.getString("contract_duration"));
					  	procurementList.setPublish_date(jsonobject.getString("publish_date"));
					  	procurementList.setContact_person(jsonobject.getString("contact_person"));
					  	procurementList.setBid_id(jsonobject.getString("_id"));
					    
						list2.add(procurementList);
					}
					dbm.clearAllData(dbm, Model_Procurement_List.TABLE);
					dbm.putData(dbm, Model_Procurement_List.TABLE, list2, mode);

		            setTitle(category);
		            ADAPTER_PROCUREMENTLISTACTIVE= new Adapter_ProcurementListActive	    (getApplicationContext(), dbm.GetProcurementListActive(dbm, ""),this);;
					
		            if(flipperHolder.getDisplayedChild() == 1){
					   listProcurementsList.setAdapter(ADAPTER_PROCUREMENTLISTACTIVE);
						mDrawerToggle.setDrawerIndicatorEnabled(false);
						listCategoryMerchant.setVisibility(View.GONE);
				        listProcurementsList.setVisibility(View.VISIBLE);
		            }
		            else{
		            	listProcurementsListPerProject.setAdapter(ADAPTER_PROCUREMENTLISTACTIVE);
							mDrawerToggle.setDrawerIndicatorEnabled(false);
							listtxtProjectType.setVisibility(View.GONE);
							listProcurementsListPerProject.setVisibility(View.VISIBLE);
		            }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case Variables.APIMODE_GETORGANIZATIONDETAILSBYSUPPLIER:
			 try {
				OuterJSON = new JSONObject(result.replace("'", "*"));
		
			 OuterJSON2 = new JSONObject( OuterJSON.getString("result"));
				JSONArray user123 = new JSONArray(OuterJSON2.getString("records"));
				ArrayList<Model_Organization> list2 = new ArrayList<Model_Organization>();
				for(int i = 0; i < user123.length(); i++){
				  	JSONObject jsonobject = user123.getJSONObject(i);
				  	Model_Organization orgList = new Model_Organization();
				  	orgList.setAddress(jsonobject.getString("address1"));
				  	orgList.setCity(jsonobject.getString("city"));
				  	orgList.setFormOfOrg(jsonobject.getString("supplier_form_of_organization"));
				  	orgList.setId(jsonobject.getString("_id"));
				  	orgList.setMemberType(jsonobject.getString("member_type"));
				  	orgList.setOrgId(jsonobject.getString("org_id"));
				  	orgList.setOrgName(jsonobject.getString("org_name"));
				  	orgList.setOrgRegDate(jsonobject.getString("org_reg_date"));
				  	orgList.setOrgstatus(jsonobject.getString("org_status"));
				  	orgList.setRegion(jsonobject.getString("region"));
				  	orgList.setUniqId(jsonobject.getString("_id"));
					list2.add(orgList);
				}
				if(list2== null || list2.size() <= 0){
					txtOrganizationInfo.setText("Invalid Organization Name");
					orgId = "";
				}
				else{
					orgId = list2.get(0).getOrgId();
				txtOrganizationInfo.setText(Html.fromHtml("<b>Org. Name: </b>"+list2.get(0).getOrgName() +"<br /><br />"+
											"<b>Form of Org.: </b>"+list2.get(0).getFormOfOrg() +"<br /><br />"+
											"<b>Address: </b>"+list2.get(0).getAddress() +"<br /><br />"+
											"<b>Cty: </b>"+list2.get(0).getCity() +"<br /><br />"+
											"<b>Org. Reg. Date: </b>"+list2.get(0).getOrgRegDate()+"<br /><br />"+
											"<b>Region: </b>"+list2.get(0).getRegion()+"<br /><br />"+
											"<b>Status: </b>"+list2.get(0).getOrgstatus()+"<br /><br />"));
				}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		break;
		case Variables.APIMODE_GETORGANIZATIONTRACKRECORDSBYSUPPLIER:
			try {
				 OuterJSON = new JSONObject(result.replace("'", "*"));
				 OuterJSON2 = new JSONObject( OuterJSON.getString("result"));
					JSONArray user123 = new JSONArray(OuterJSON2.getString("records"));
					ArrayList<Model_Procurement_List> list2 = new ArrayList<Model_Procurement_List>();
					for(int i = 0; i < user123.length(); i++){
					  	JSONObject jsonobject = user123.getJSONObject(i);
					  	Model_Procurement_List procurementList = new Model_Procurement_List();
					  	procurementList.setBusinessCategory(jsonobject.getString("business_category"));
					  	procurementList.setClassification(jsonobject.getString("classification"));
					  	procurementList.setFunding_instrument(jsonobject.getString("funding_instrument"));
					  	procurementList.setCalendar_type(jsonobject.getString("calendar_type"));
					  	procurementList.setRef_id(jsonobject.getString("ref_id"));
					  	procurementList.setNotice_type(jsonobject.getString("notice_type"));
					  	procurementList.setFunding_source(jsonobject.getString("funding_source"));
					  	procurementList.setModified_date(jsonobject.getString("modified_date"));
					  	procurementList.setProcurement_mode(jsonobject.getString("procurement_mode"));
					  	procurementList.setCreated_by(jsonobject.getString("created_by"));
					  	procurementList.setContact_person_address(jsonobject.getString("contact_person_address"));
					  	procurementList.setApproved_budget(jsonobject.getString("approved_budget"));
					  	procurementList.setDescription(jsonobject.getString("description"));
					  	procurementList.setProcuring_entity_org(jsonobject.getString("procuring_entity_org"));
					  	procurementList.setCreation_date(jsonobject.getString("creation_date"));
					  	procurementList.setTrade_agreement(jsonobject.getString("trade_agreement"));
					  	procurementList.setTender_title(jsonobject.getString("tender_title"));
					  	procurementList.setClosing_date(jsonobject.getString("closing_date"));
					  	procurementList.setTender_status(jsonobject.getString("tender_status"));
					  	procurementList.setContract_duration(jsonobject.getString("contract_duration"));
					  	procurementList.setPublish_date(jsonobject.getString("publish_date"));
					  	procurementList.setContact_person(jsonobject.getString("contact_person"));
					  	procurementList.setBid_id(jsonobject.getString("_id"));
					    
						list2.add(procurementList);
					}
					dbm.clearAllData(dbm, Model_Procurement_List.TABLE);
					dbm.putData(dbm, Model_Procurement_List.TABLE, list2, Variables.APIMODE_GETACTIVEPROCUREMENTBYCATEGORY);
					
		            ADAPTER_PROCUREMENTLISTHISTORY= new Adapter_ProcurementListHistory	    (getApplicationContext(), dbm.GetProcurementListActive(dbm, ""),this);;
				    listOrganizationHistory.setAdapter(ADAPTER_PROCUREMENTLISTHISTORY);
				    if(ADAPTER_PROCUREMENTLISTHISTORY.getCount() > 0)
				    	flipperSupplier.setDisplayedChild(0);
				    else{
			            flipperSupplier.setDisplayedChild(1);
			            Toast.makeText(getApplicationContext(), "NO Data Found!", Toast.LENGTH_SHORT).show();
				    }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
	    int itemId = item.getItemId();
	    switch (itemId) {
	    case android.R.id.home:
			if(mDrawerToggle.isDrawerIndicatorEnabled()){
				if(!mDrawerLayout.isDrawerOpen(layoutDrawer))
					mDrawerLayout.openDrawer(Gravity.LEFT);
				else
					mDrawerLayout.closeDrawer(Gravity.LEFT);
		    }
		    else
		    {
		    	//back press
		    	if(flipperHolder.getDisplayedChild() == 1){
		    		mDrawerToggle.setDrawerIndicatorEnabled(true);
		    		listProcurementsList.setVisibility(View.GONE);
		    		listCategoryMerchant.setVisibility(View.VISIBLE);
		            setTitle("Opportunities");
		    	}
		    	else if(flipperHolder.getDisplayedChild() == 2){
		    		mDrawerToggle.setDrawerIndicatorEnabled(true);
		    		listProcurementsListPerProject.setVisibility(View.GONE);
		    		listtxtProjectType.setVisibility(View.VISIBLE);
		            setTitle("Budget Allocation");
		    	}
		     }
	    break;
	
	    }
	    return true;
	}

	@Override
	public void OnCustomClick(View aView, String extra, int itemPosition,
			int mode) {
		// TODO Auto-generated method stub
		switch (mode){

		case Variables.CLICKLISTENER_ID_MERCHANTCATEGORY:
	      	myAsyncTask = new WebAPIAsyntask(Variables.APIMODE_GETACTIVEPROCUREMENTBYCATEGORY, API, getApplicationContext(),EventListener.this,extra);
			myAsyncTask.execute();
			break;
		case Variables.CLICKLISTENER_ID_PROCUREMENTACTIVE:
			List<Model_Procurement_List> instructionslist = dbm.GetProcurementListActiveById(dbm, Integer.toString(itemPosition));
			CLASS_DIALOG.groceryListAddDialog(EventListener.this, EventListener.this, "",instructionslist, 1);
			break;
	
		}
	}
	public void btnSearchClick(View view){
		myAsyncTask = new WebAPIAsyntask(Variables.APIMODE_GETORGANIZATIONDETAILSBYSUPPLIER, API, getApplicationContext(),EventListener.this,txtSupplier.getText().toString());
		myAsyncTask.execute();
	}
	public void btnHistoryClick(View view){
		if(orgId.length()>0 || orgId != null){
		myAsyncTask = new WebAPIAsyntask(Variables.APIMODE_GETORGANIZATIONTRACKRECORDSBYSUPPLIER, API, getApplicationContext(),EventListener.this,orgId);
		myAsyncTask.execute();
		}
	}
	public void btnInfoClick(View view){
		flipperSupplier.setDisplayedChild(1);
	}
	public void drawerClick(View view){
    	drawerIndicator.setY(view.getY());
    	switch(view.getId()){
    		case R.id.btnHome:
    			selectItem(0);
    		break;
    		case R.id.btnOpportunities:
            	selectItem(1);
        	break;
    		case R.id.btnBudgetAllocation:
            	selectItem(2);
        	break;
    		case R.id.btnSupplier:
            	selectItem(3);
    			break;
    	}
    }
	public void selectItem(int position) {
        
    	flipperHolder.setDisplayedChild(position);
    	switch(position){
    	case 0:
    		flipperHolder.setDisplayedChild(position);
            setTitle("BidWatch");
            flipperHolder.setDisplayedChild(0);
			mDrawerToggle.setDrawerIndicatorEnabled(true);
    	break;
    	case 1:
    		flipperHolder.setDisplayedChild(position);
            setTitle("Opportunities");
            flipperHolder.setDisplayedChild(1);
			mDrawerToggle.setDrawerIndicatorEnabled(true);

			//mDrawerToggle.setDrawerIndicatorEnabled(false);
    	break;
    		
    	case 2:
    		flipperHolder.setDisplayedChild(position);
            setTitle("Budget Allocation");
            flipperHolder.setDisplayedChild(2);
			mDrawerToggle.setDrawerIndicatorEnabled(true);
    	break;
     	case 3:
    		flipperHolder.setDisplayedChild(position);
            setTitle("Supplier");
            flipperHolder.setDisplayedChild(3);
			mDrawerToggle.setDrawerIndicatorEnabled(true);
    	break;
    	}
        hideKeyboard();
        mDrawerLayout.closeDrawer(layoutDrawer);
    }

	
	 public void hideKeyboard(){
			InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
