package ph.helios.bidwatch.listener;


import android.view.View;
import android.view.View.OnClickListener;

public class CustomOnClickListener  implements OnClickListener {
	private int mode;
	private int itemID;
	private String mExtras;
	private OnInterfaceClickListener callback;
	
   // Pass in the callback (this'll be the activity) and the row position
	public CustomOnClickListener(OnInterfaceClickListener callback,String extras,int...params) {
		itemID	  	  = params[0];
		mode		  = params[1];
		mExtras 	  = extras;
		this.callback = callback;
	}

	@Override
	public void onClick(View v) {
        // Let's call our custom callback with the position we added in the constructor
		callback.OnCustomClick(v,mExtras, itemID,mode);
	}
}