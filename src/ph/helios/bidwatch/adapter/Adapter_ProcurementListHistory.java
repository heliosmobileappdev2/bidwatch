package ph.helios.bidwatch.adapter;

import java.util.ArrayList;
import ph.helios.bidwatch.R;
import ph.helios.bidwatch.Model.Model_Procurement_List;
import ph.helios.bidwatch.listener.CustomOnClickListener;
import ph.helios.bidwatch.listener.OnInterfaceClickListener;
import ph.helios.bidwatch.utilities.Variables;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class Adapter_ProcurementListHistory extends BaseAdapter {
	
	private ArrayList<Model_Procurement_List>	 	 searchArrayList;
	private LayoutInflater							 mInflater;
	private OnInterfaceClickListener     			 mCallback; 
	
	public Adapter_ProcurementListHistory(Context context, ArrayList<Model_Procurement_List> results, OnInterfaceClickListener callback) {
		searchArrayList	 = results;
		mInflater		 = LayoutInflater.from(context);
		mCallback		 = callback;
	}
	public int getCount() {
		return searchArrayList.size();
	}
	public Object getItem(int position) {
		return searchArrayList.get(position);
	}
	public long getItemId(int position) {
		return position;
	}
	public void updateAdapter(ArrayList<Model_Procurement_List> results) {
        searchArrayList = results;
        notifyDataSetChanged();
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder lv_holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_procurementshistory, null);
			lv_holder = new ViewHolder();
			
			lv_holder.txtTitle		 = (TextView)convertView.findViewById(R.id.txtTitle);
			lv_holder.txtDate		 = (TextView)convertView.findViewById(R.id.txtDate);
			lv_holder.txtShortDescripton		 = (TextView)convertView.findViewById(R.id.txtShortDescripton);
			lv_holder.btnContinueReading		 = (ImageButton)convertView.findViewById(R.id.btnContinueReading);
			
			convertView.setTag(lv_holder);
		} else {
			lv_holder = (ViewHolder) convertView.getTag();
		}
		lv_holder.txtTitle.setTag(searchArrayList.get(position).getId());
		lv_holder.txtTitle.setText(searchArrayList.get(position).getTender_title());
		lv_holder.txtDate.setText(searchArrayList.get(position).getPublish_date());
		lv_holder.txtShortDescripton.setText(searchArrayList.get(position).getDescription());
		int id =0;
		if(searchArrayList.get(position).getId().trim().length() >0)
		 id = Integer.valueOf(searchArrayList.get(position).getId());
		lv_holder.btnContinueReading.setOnClickListener(new CustomOnClickListener(mCallback,searchArrayList.get(position).getDescription(), id,Variables.CLICKLISTENER_ID_PROCUREMENTACTIVE));
		
		return convertView;
	}

	class ViewHolder {
		TextView txtTitle,txtDate,txtShortDescripton;
		ImageButton btnContinueReading;
	}
	
	
}