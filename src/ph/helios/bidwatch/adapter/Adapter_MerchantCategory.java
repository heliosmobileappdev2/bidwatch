package ph.helios.bidwatch.adapter;

import java.util.ArrayList;
import ph.helios.bidwatch.R;
import ph.helios.bidwatch.Model.Model_MerchantCategories;
import ph.helios.bidwatch.listener.CustomOnClickListener;
import ph.helios.bidwatch.listener.OnInterfaceClickListener;
import ph.helios.bidwatch.utilities.Variables;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class Adapter_MerchantCategory extends BaseAdapter {
	
	private ArrayList<Model_MerchantCategories>	 	 searchArrayList;
	private LayoutInflater					 mInflater;
	private OnInterfaceClickListener     	 mCallback; 
	
	public Adapter_MerchantCategory(Context context, ArrayList<Model_MerchantCategories> results, OnInterfaceClickListener callback) {
		searchArrayList	 = results;
		mInflater		 = LayoutInflater.from(context);
		mCallback		 = callback;
	}
	public int getCount() {
		return searchArrayList.size();
	}
	public Object getItem(int position) {
		return searchArrayList.get(position);
	}
	public long getItemId(int position) {
		return position;
	}
	public void updateAdapter(ArrayList<Model_MerchantCategories> results) {
        searchArrayList = results;
        notifyDataSetChanged();
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder lv_holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_merchantcategories, null);
			lv_holder = new ViewHolder();
			
			lv_holder.txt1		 = (TextView)convertView.findViewById(R.id.txt1);
			lv_holder.ivNext	 = (ImageButton)convertView.findViewById(R.id.ivNext);

			convertView.setTag(lv_holder);
		} else {
			lv_holder = (ViewHolder) convertView.getTag();
		}
		lv_holder.ivNext.setTag(searchArrayList.get(position).getId());
		lv_holder.txt1.setTag(searchArrayList.get(position).getId());
		lv_holder.txt1.setText(searchArrayList.get(position).getCategory());
		lv_holder.ivNext.setOnClickListener(new CustomOnClickListener(mCallback,searchArrayList.get(position).getCategory(), Integer.valueOf(searchArrayList.get(position).getId()),Variables.CLICKLISTENER_ID_MERCHANTCATEGORY));
		lv_holder.txt1.setOnClickListener(new CustomOnClickListener(mCallback,searchArrayList.get(position).getCategory(), Integer.valueOf(searchArrayList.get(position).getId()),Variables.CLICKLISTENER_ID_MERCHANTCATEGORY));
		
		return convertView;
	}

	class ViewHolder {
		TextView txt1;
		ImageButton ivNext;
	}
	
	
}