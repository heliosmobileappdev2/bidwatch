package ph.helios.bidwatch.database;

import java.util.ArrayList;
import java.util.List;

import ph.helios.bidwatch.Model.Model_MerchantCategories;
import ph.helios.bidwatch.Model.Model_Procurement_List;
import ph.helios.bidwatch.utilities.Variables;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbManager extends SQLiteOpenHelper {
	
	public DbManager(Context context) {
		super(context, "dbherehere23", null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		 db.execSQL(Model_MerchantCategories.createQuerry);
		 db.execSQL(Model_Procurement_List.createQuerry);
		 
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(Model_MerchantCategories.dropQuerry);
		db.execSQL(Model_Procurement_List.dropQuerry);
		onCreate(db);
	}
	
	
	public void putData(DbManager DBMANAGER,String table, List<?> models, int mode){
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		db.beginTransaction();
		String InsertString1;
		switch (mode){
		
			case Variables.APIMODE_GETMERCHANTCATEGORY:
				InsertString1 = "INSERT INTO " + table + " (" + Model_MerchantCategories.Columns + ") values(";	
			    for (Object SearchResult : models) {	
			    	StringBuilder sb2 = new StringBuilder(InsertString1);
										    sb2.append("'");
											sb2.append(((Model_MerchantCategories) SearchResult).getTotolAllotedBudget()+ "','");
											sb2.append(((Model_MerchantCategories) SearchResult).getCategory()		+ "')");
					db.execSQL(sb2.toString());
				}
			break;
			case Variables.APIMODE_GETACTIVEPROCUREMENTBYCATEGORY:
				InsertString1 = "INSERT INTO " + table + " (" + Model_Procurement_List.Columns + ") values(";	
			    for (Object SearchResult : models) {	
			    	StringBuilder sb2 = new StringBuilder(InsertString1);
										    sb2.append("'");
											sb2.append(((Model_Procurement_List) SearchResult).getBusinessCategory()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getClassification()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getFunding_instrument()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getCalendar_type()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getRef_id()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getNotice_type()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getFunding_source()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getModified_date()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getProcurement_mode()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getCreated_by()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getContact_person_address()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getApproved_budget()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getDescription()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getProcuring_entity_org()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getCreation_date()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getTrade_agreement()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getTender_title()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getClosing_date()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getTender_status()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getContract_duration()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getPublish_date()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getContact_person()		+ "','");
											sb2.append(((Model_Procurement_List) SearchResult).getBid_id()		+ "')");
					db.execSQL(sb2.toString());
				}
			    
			break;
		} 
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
	}
	
	
	public ArrayList<Model_Procurement_List> GetProcurementListActive(DbManager DBMANAGER, String...params) {
		ArrayList<Model_Procurement_List> results = new ArrayList<Model_Procurement_List>();
	
		Model_Procurement_List sr1 = new Model_Procurement_List();
		String sql = "Select * "+" from " + Model_Procurement_List.TABLE +" where "+ Model_Procurement_List.TENDERTITLE + " like '%"
					+params[0]+"%' order by " + Model_Procurement_List.TENDERTITLE +" ASC";
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
	
		while (cursor.moveToNext()) {
		     	sr1 = new Model_Procurement_List();
			  	sr1.setId(cursor.getString(0));
			  	sr1.setBusinessCategory(cursor.getString(1));
			  	sr1.setClassification(cursor.getString(2));
			  	sr1.setFunding_instrument(cursor.getString(3));
			  	sr1.setCalendar_type(cursor.getString(4));
			  	sr1.setRef_id(cursor.getString(5));
			  	sr1.setNotice_type(cursor.getString(6));
			  	sr1.setFunding_source(cursor.getString(7));
			  	sr1.setModified_date(cursor.getString(8));
			  	sr1.setProcurement_mode(cursor.getString(9));
			  	sr1.setCreated_by(cursor.getString(10));
			  	sr1.setContact_person_address(cursor.getString(11));
			  	sr1.setApproved_budget(cursor.getString(12));
			  	sr1.setDescription(cursor.getString(13));
			  	sr1.setProcuring_entity_org(cursor.getString(14));
			  	sr1.setCreation_date(cursor.getString(15));
			  	sr1.setTrade_agreement(cursor.getString(16));
			  	sr1.setTender_title(cursor.getString(17));
			  	sr1.setClosing_date(cursor.getString(18));
			  	sr1.setTender_status(cursor.getString(19));
			  	sr1.setContract_duration(cursor.getString(20));
			  	sr1.setPublish_date(cursor.getString(21));
			  	sr1.setContact_person(cursor.getString(22));
			  	sr1.setBid_id(cursor.getString(23));
				results.add(sr1);
		}
		cursor.close();
		db.close();
		return results;
	}
	
	public ArrayList<Model_Procurement_List> GetProcurementListHistory(DbManager DBMANAGER, String...params) {
		ArrayList<Model_Procurement_List> results = new ArrayList<Model_Procurement_List>();
	
		Model_Procurement_List sr1 = new Model_Procurement_List();
		String sql = "Select * "+" from " + Model_Procurement_List.TABLE +" where "+ Model_Procurement_List.TENDERTITLE + " like '%"
					+params[0]+"%' order by " + Model_Procurement_List.TENDERTITLE +" ASC";
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
	
		while (cursor.moveToNext()) {
		     	sr1 = new Model_Procurement_List();
			  	sr1.setId(cursor.getString(0));
			  	sr1.setBusinessCategory(cursor.getString(1));
			  	sr1.setClassification(cursor.getString(2));
			  	sr1.setFunding_instrument(cursor.getString(3));
			  	sr1.setCalendar_type(cursor.getString(4));
			  	sr1.setRef_id(cursor.getString(5));
			  	sr1.setNotice_type(cursor.getString(6));
			  	sr1.setFunding_source(cursor.getString(7));
			  	sr1.setModified_date(cursor.getString(8));
			  	sr1.setProcurement_mode(cursor.getString(9));
			  	sr1.setCreated_by(cursor.getString(10));
			  	sr1.setContact_person_address(cursor.getString(11));
			  	sr1.setApproved_budget(cursor.getString(12));
			  	sr1.setDescription(cursor.getString(13));
			  	sr1.setProcuring_entity_org(cursor.getString(14));
			  	sr1.setCreation_date(cursor.getString(15));
			  	sr1.setTrade_agreement(cursor.getString(16));
			  	sr1.setTender_title(cursor.getString(17));
			  	sr1.setClosing_date(cursor.getString(18));
			  	sr1.setTender_status(cursor.getString(19));
			  	sr1.setContract_duration(cursor.getString(20));
			  	sr1.setPublish_date(cursor.getString(21));
			  	sr1.setContact_person(cursor.getString(22));
			  	sr1.setBid_id(cursor.getString(23));
				results.add(sr1);
		}
		cursor.close();
		db.close();
		return results;
	}
	
	public ArrayList<Model_Procurement_List> GetProcurementListActiveById(DbManager DBMANAGER, String...params) {
		ArrayList<Model_Procurement_List> results = new ArrayList<Model_Procurement_List>();
	
		Model_Procurement_List sr1 = new Model_Procurement_List();
		String sql = "Select * "+" from " + Model_Procurement_List.TABLE +" where "+ Model_Procurement_List.ID + " = '"
					+params[0]+"' ";
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
	
		while (cursor.moveToNext()) {
		     	sr1 = new Model_Procurement_List();
			  	sr1.setId(cursor.getString(0));
			  	sr1.setBusinessCategory(cursor.getString(1));
			  	sr1.setClassification(cursor.getString(2));
			  	sr1.setFunding_instrument(cursor.getString(3));
			  	sr1.setCalendar_type(cursor.getString(4));
			  	sr1.setRef_id(cursor.getString(5));
			  	sr1.setNotice_type(cursor.getString(6));
			  	sr1.setFunding_source(cursor.getString(7));
			  	sr1.setModified_date(cursor.getString(8));
			  	sr1.setProcurement_mode(cursor.getString(9));
			  	sr1.setCreated_by(cursor.getString(10));
			  	sr1.setContact_person_address(cursor.getString(11));
			  	sr1.setApproved_budget(cursor.getString(12));
			  	sr1.setDescription(cursor.getString(13));
			  	sr1.setProcuring_entity_org(cursor.getString(14));
			  	sr1.setCreation_date(cursor.getString(15));
			  	sr1.setTrade_agreement(cursor.getString(16));
			  	sr1.setTender_title(cursor.getString(17));
			  	sr1.setClosing_date(cursor.getString(18));
			  	sr1.setTender_status(cursor.getString(19));
			  	sr1.setContract_duration(cursor.getString(20));
			  	sr1.setPublish_date(cursor.getString(21));
			  	sr1.setContact_person(cursor.getString(22));
			  	sr1.setBid_id(cursor.getString(23));
				results.add(sr1);
		}
		cursor.close();
		db.close();
		return results;
	}
	
	public ArrayList<Model_MerchantCategories> GetMerchantCategories(DbManager DBMANAGER, String...params) {
		ArrayList<Model_MerchantCategories> results = new ArrayList<Model_MerchantCategories>();
	
		Model_MerchantCategories sr1 = new Model_MerchantCategories();
		String sql = "Select * "+" from " + Model_MerchantCategories.TABLE +" where "+ Model_MerchantCategories.CATEGORY + " like '%"
					+params[0]+"%' order by " + Model_MerchantCategories.CATEGORY +" ASC";
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
	
		while (cursor.moveToNext()) {
		     	sr1 = new Model_MerchantCategories();
				sr1.setId(cursor.getString(0));
				sr1.setTotolAllotedBudget(cursor.getString(1));
				sr1.setCategory(cursor.getString(2));
				results.add(sr1);
		}
		cursor.close();
		db.close();
		return results;
	}
	public boolean hasData(DbManager DBMANAGER, String table) {
		String sql = "" ;
	
			 sql = "Select * from " + table;
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
		int counter = cursor.getCount();
		cursor.close();
		db.close();
		if(counter != 0) return true; else return false;
	}
	public  void clearAllData(DbManager DBMANAGER, String tableName){
		SQLiteDatabase db = DBMANAGER.getWritableDatabase();
		db.delete(tableName, null, null);
		db.close();
	}
}
