package ph.helios.bidwatch.Model;

public class Model_Procurement_List {
	
	private String id, businessCategory, classification,funding_instrument,calendar_type,ref_id,notice_type,funding_source,
			modified_date, procurement_mode, created_by, contact_person_address, approved_budget, description, procuring_entity_org,
			creation_date, trade_agreement, tender_title, closing_date, tender_status, contract_duration, publish_date, contact_person,
			bid_id;

	public static final String TABLE				= "tblProcurementList";
	public static final String ID					= "_id";
	public static final String BUSINESSCATEGORY		= "businessCategory";
	public static final String CLASSIFICATION		= "classification";
	public static final String FUNDINGINSTRUMENT	= "fundinginstrument";
	public static final String CALENDARTYPE			= "calendartype";
	public static final String REFID				= "refid";
	public static final String NOTICETYPE			= "noticetype";
	public static final String FUNDINGSOURCE		= "fundingsource";
	public static final String MODIFIEDDATE			= "modifieddate";
	public static final String PROCUREMENTMODE		= "procurementmode";
	public static final String CREATEDBY			= "createdby";
	public static final String CONTACTPERSONADDRESS	= "contactaddress";
	public static final String APPROVEDBUDGET		= "approvedbudget";
	public static final String DESCRIPTION			= "description";
	public static final String PROCURINGENTITYORG	= "procuringentityorg";
	public static final String CREATIONDATE			= "creationdate";
	public static final String TRADEAGREEMENT		= "tradeagreement";
	public static final String TENDERTITLE			= "tendertitle";
	public static final String CLOSINGDATE			= "closingDate";
	public static final String TENDERSTATUS			= "tenderStatus";
	public static final String CONTRACTDURATION		= "contractduration";
	public static final String PUBLISHDATE			= "publishDate";
	public static final String CONTACTPERSON		= "contactPerson";
	public static final String BIDID				= "bidId";

	public static final String Columns 		 		=  BUSINESSCATEGORY+ "," + CLASSIFICATION + "," +FUNDINGINSTRUMENT + "," +
													   CALENDARTYPE+ "," + REFID + "," +NOTICETYPE + "," +
													   FUNDINGSOURCE+ "," + MODIFIEDDATE + "," +PROCUREMENTMODE + "," +
													   CREATEDBY+ "," + CONTACTPERSONADDRESS + "," +APPROVEDBUDGET + "," +
													   DESCRIPTION+ "," + PROCURINGENTITYORG + "," +CREATIONDATE + "," +
													   TRADEAGREEMENT+ "," + TENDERTITLE + "," +CLOSINGDATE + ","+
													   TENDERSTATUS+ "," + CONTRACTDURATION + "," +PUBLISHDATE + ","+
													   CONTACTPERSON+ "," + BIDID;
	
	public static final String dropQuerry 	 		= "drop table if exists "+ TABLE;
	
	public static final String createQuerry 		= "create table " 			+ TABLE + "(" +
														ID						+ " integer primary key autoincrement , "+
														BUSINESSCATEGORY						+ " text null , "+
														CLASSIFICATION						+ " text null , "+
														FUNDINGINSTRUMENT						+ " text null , "+
														CALENDARTYPE						+ " text null , "+
														REFID						+ " text null , "+
														NOTICETYPE						+ " text null , "+
														FUNDINGSOURCE						+ " text null , "+
														MODIFIEDDATE						+ " text null , "+
														PROCUREMENTMODE						+ " text null , "+
														CREATEDBY						+ " text null , "+
														CONTACTPERSONADDRESS						+ " text null , "+
														APPROVEDBUDGET						+ " text null , "+
														DESCRIPTION						+ " text null , "+
														PROCURINGENTITYORG						+ " text null , "+
														CREATIONDATE						+ " text null , "+
														TRADEAGREEMENT						+ " text null , "+
														TENDERTITLE						+ " text null , "+
														CLOSINGDATE						+ " text null , "+
														TENDERSTATUS						+ " text null , "+
														CONTRACTDURATION						+ " text null , "+
														PUBLISHDATE						+ " text null , "+
														CONTACTPERSON						+ " text null , "+
														BIDID					+ " text null)";


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBusinessCategory() {
		return businessCategory;
	}

	public void setBusinessCategory(String businessCategory) {
		this.businessCategory = businessCategory;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getFunding_instrument() {
		return funding_instrument;
	}

	public void setFunding_instrument(String funding_instrument) {
		this.funding_instrument = funding_instrument;
	}

	public String getCalendar_type() {
		return calendar_type;
	}

	public void setCalendar_type(String calendar_type) {
		this.calendar_type = calendar_type;
	}

	public String getRef_id() {
		return ref_id;
	}

	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public String getNotice_type() {
		return notice_type;
	}

	public void setNotice_type(String notice_type) {
		this.notice_type = notice_type;
	}

	public String getFunding_source() {
		return funding_source;
	}

	public void setFunding_source(String funding_source) {
		this.funding_source = funding_source;
	}

	public String getModified_date() {
		return modified_date;
	}

	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}

	public String getProcurement_mode() {
		return procurement_mode;
	}

	public void setProcurement_mode(String procurement_mode) {
		this.procurement_mode = procurement_mode;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public String getTrade_agreement() {
		return trade_agreement;
	}

	public void setTrade_agreement(String trade_agreement) {
		this.trade_agreement = trade_agreement;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getContact_person_address() {
		return contact_person_address;
	}

	public void setContact_person_address(String contact_person_address) {
		this.contact_person_address = contact_person_address;
	}

	public String getApproved_budget() {
		return approved_budget;
	}

	public void setApproved_budget(String approved_budget) {
		this.approved_budget = approved_budget;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProcuring_entity_org() {
		return procuring_entity_org;
	}

	public void setProcuring_entity_org(String procuring_entity_org) {
		this.procuring_entity_org = procuring_entity_org;
	}

	public String getTender_title() {
		return tender_title;
	}

	public void setTender_title(String tender_title) {
		this.tender_title = tender_title;
	}

	public String getClosing_date() {
		return closing_date;
	}

	public void setClosing_date(String closing_date) {
		this.closing_date = closing_date;
	}

	public String getTender_status() {
		return tender_status;
	}

	public void setTender_status(String tender_status) {
		this.tender_status = tender_status;
	}

	public String getContract_duration() {
		return contract_duration;
	}

	public void setContract_duration(String contract_duration) {
		this.contract_duration = contract_duration;
	}

	public String getPublish_date() {
		return publish_date;
	}

	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}

	public String getContact_person() {
		return contact_person;
	}

	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	public String getBid_id() {
		return bid_id;
	}

	public void setBid_id(String bid_id) {
		this.bid_id = bid_id;
	}
}
