package ph.helios.bidwatch.Model;

public class Model_MerchantCategories {

	private String id, category, totolAllotedBudget;
	
	public static final String TABLE				= "tblMerchantCategories";
	public static final String ID					= "_id";
	public static final String CATEGORY				= "category";
	public static final String TOTALALLOTEDBUDGET	= "totalallotedbudget";

	public static final String Columns 		 		=  TOTALALLOTEDBUDGET+","+CATEGORY;
	public static final String dropQuerry 	 		= "drop table if exists "+ TABLE;
	
	public static final String createQuerry 		= "create table " 			+ TABLE + "(" +
														ID						+ " integer primary key autoincrement , "+
														TOTALALLOTEDBUDGET			+ " text not null , "+
														CATEGORY					+ " text not null)";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTotolAllotedBudget() {
		return totolAllotedBudget;
	}

	public void setTotolAllotedBudget(String totolAllotedBudget) {
		this.totolAllotedBudget = totolAllotedBudget;
	}
	
}
