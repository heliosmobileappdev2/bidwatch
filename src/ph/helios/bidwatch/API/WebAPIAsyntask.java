package ph.helios.bidwatch.API;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import ph.helios.bidwatch.utilities.Variables;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class WebAPIAsyntask extends AsyncTask<Void, Void, String> {
	
	APIListener mListener;
	Context mContext;
	int mAPIMode;
	String[] mParams;
	WebAPI mAPI;
	
	public interface APIListener {
		void onAPIStarted(int mode);
		void onAPIFinished(String result, int mode, String...Params);
	}

	public WebAPIAsyntask(int apiMode, WebAPI API,Context context,APIListener callback,String... Params){
	    this.mAPIMode = apiMode;
	    this.mParams = Params;
	    this.mAPI = API;
	    this.mContext = context;
	    this.mListener = callback;
	}
	
	protected void onPreExecute() {
		mListener.onAPIStarted(mAPIMode);
	}
	
	protected void onPostExecute(final String result) {
		mListener.onAPIFinished(result,mAPIMode,mParams);
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		String url = "";
		switch(mAPIMode){
			case Variables.APIMODE_GETMERCHANTCATEGORY:
				url = Variables.STR_ENDPOINT+Variables.URL_GET_MERCHANTCATEGORY;
				url = url.replace(" ", "%20").replace("'", "%27").replace(",", "%2C").replace("&", "%26");
				return mAPI.doAPIRequest(mAPIMode,true, url);
			case Variables.APIMODE_GETACTIVEPROCUREMENTBYCATEGORY:
			try {
				String extras = " limit 50" ;
				url = Variables.STR_ENDPOINT+Variables.URL_GET_ACTIVEPROCUREMENTBYCATEGORY + URLEncoder.encode(mParams[0], "utf-8") +"'"+extras;
				url = url.replace(" ", "%20").replace("'", "%27").replace(",", "%2C").replace("&", "%26");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				return mAPI.doAPIRequest(mAPIMode,true, url);
			case Variables.APIMODE_GETLOCATIONIDBYDETAILS:
				try {
					String extras = " limit 50" ;
					url = Variables.STR_ENDPOINT+Variables.URL_GET_LOCATIONIDBYDETAILS + URLEncoder.encode(mParams[0], "utf-8") +"'"+extras;
					url = url.replace(" ", "%20").replace("'", "%27").replace(",", "%2C").replace("&", "%26");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return mAPI.doAPIRequest(mAPIMode,true, url);
			case Variables.APIMODE_GETORGANIZATIONDETAILSBYSUPPLIER:
				try {
					String extras = " limit 50" ;
					url = Variables.STR_ENDPOINT+Variables.URL_GET_ORGANIZATIONDETAILSBYSUPPLIER + URLEncoder.encode(mParams[0], "utf-8") +"'"+extras;
					url = url.replace(" ", "%20").replace("'", "%27").replace(",", "%2C").replace("&", "%26");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return mAPI.doAPIRequest(mAPIMode,true, url);
			case Variables.APIMODE_GETORGANIZATIONTRACKRECORDSBYSUPPLIER:
				try {
					String extras = " limit 50" ;
					url = Variables.STR_ENDPOINT+Variables.URL_GET_ORGANIZATIONTRACKRECORDSBYSUPPLIER + URLEncoder.encode(mParams[0], "utf-8") +"'"+extras;
					url = url.replace(" ", "%20").replace("'", "%27").replace(",", "%2C").replace("&", "%26");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return mAPI.doAPIRequest(mAPIMode,true, url);
				
				
			default:
				return "Error Occured";
		}
	}
}