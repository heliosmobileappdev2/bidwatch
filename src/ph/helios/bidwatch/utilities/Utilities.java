package ph.helios.bidwatch.utilities;

import android.content.res.AssetManager;
import android.graphics.Typeface;

public class Utilities {
	
	public static final String STR_FONTS_ACCENGRALIG 			= "font/orbitron.ttf";
	
	public static Typeface FontStyle(AssetManager assetmanager){
		Typeface face;
			face = Typeface.createFromAsset(assetmanager, Utilities.STR_FONTS_ACCENGRALIG);
			return face;
		
	}
	

}
