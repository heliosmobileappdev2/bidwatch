package ph.helios.bidwatch.utilities;

import ph.helios.bidwatch.API.WebAPI;
import ph.helios.bidwatch.API.WebAPIAsyntask;
import ph.helios.bidwatch.adapter.Adapter_MerchantCategory;
import ph.helios.bidwatch.adapter.Adapter_ProcurementListActive;
import ph.helios.bidwatch.adapter.Adapter_ProcurementListHistory;
import ph.helios.bidwatch.adapter.Adapter_ProjectType;
import ph.helios.bidwatch.database.DbManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class Variables extends Activity {

	public static final int FlipperMode_SignOrCreate		 					= 0;

	public static final int APIMODE_GETMERCHANTCATEGORY						    = 200;
	public static final int APIMODE_GETACTIVEPROCUREMENTBYCATEGORY				= 201;
	public static final int APIMODE_GETLOCATIONIDBYDETAILS						= 202;
	public static final int APIMODE_GETORGANIZATIONDETAILSBYSUPPLIER			= 203;
	public static final int APIMODE_GETORGANIZATIONTRACKRECORDSBYSUPPLIER		= 204;

	public static final int CLICKLISTENER_ID_MERCHANTCATEGORY		 	   		= 1000;
	public static final int CLICKLISTENER_ID_PROCUREMENTACTIVE		 	   		= 1001;
	public static final int CLICKLISTENER_ID_PROCUREMENTPROJECTTYPE		 	   	= 1002;


	public static final String FONTSTYLE_MAVEN_REGULAR 							= "fonts/maven_regular.ttf";
	
	public static final String URL_GET_MERCHANTCATEGORY							= "Select%20business_category,%20SUM(approved_budget)%20from%20%22baccd784-45a2-4c0c-82a6-61694cd68c9d%22%20GROUP%20BY%20business_category";
	public static final String URL_GET_ACTIVEPROCUREMENTBYCATEGORY				= "SELECT%20*%20from%20%22baccd784-45a2-4c0c-82a6-61694cd68c9d%22%20where%20business_category%20=%27";
	public static final String URL_GET_LOCATIONIDBYDETAILS						= "Select%20*%20from%20%22116b0812-23b4-4a92-afcc-1030a0433108%22%20where%20location%20=%20%27";
	public static final String URL_GET_ACTIVEPROCUREMENTBYLOCATIONID			= "Select%20*%20from%20%22baccd784-45a2-4c0c-82a6-61694cd68c9d%22%20where%20ref_id%20=%20%277";
	public static final String URL_GET_ORGANIZATIONDETAILSBYSUPPLIER			= "Select%20*%20from%20%22ec10e1c4-4eb3-4f29-97fe-f09ea950cdf1%22%20where%20org_name=%27";
	public static final String URL_GET_ORGANIZATIONTRACKRECORDSBYSUPPLIER		= "Select%20*%20from%20%22baccd784-45a2-4c0c-82a6-61694cd68c9d%22%20where%20org_id=%27";

	public DbManager dbm;
	public static final String STR_ENDPOINT 									= "http://philgeps.cloudapp.net:5000/api/action/datastore_search_sql?sql=";	
	
	public Utilities Utilitiess;
	
	
	public ProgressDialog dialog;
	
	public String orgId = "";
	 public DrawerLayout mDrawerLayout;
	 public ActionBarDrawerToggle mDrawerToggle;

	 public CharSequence mDrawerTitle;
	 public CharSequence mTitle;
	 
	 public ViewFlipper flipperHolder;
	 public ViewFlipper flipperSupplier;
	 
	 
	 public LinearLayout layoutDrawer;
	 public LinearLayout drawerIndicator;
	 public Typeface  mTypeface;

		public CustomDialog	CLASS_DIALOG;
	 public Adapter_MerchantCategory					ADAPTER_MERCHANTCATEGORIES;
	 public Adapter_ProjectType							ADAPTER_PROJECTTYPE;
	 public Adapter_ProcurementListActive					ADAPTER_PROCUREMENTLISTACTIVE;
	 public Adapter_ProcurementListHistory					ADAPTER_PROCUREMENTLISTHISTORY;
	 
		public WebAPI API;
		public WebAPIAsyntask myAsyncTask;
		public ListView listCategoryMerchant;
		public ListView listProcurementsList;
		public ListView listProcurementsListPerProject;
		
		public ListView listtxtProjectType;
		public ListView listOrganizationHistory;
		
		public TextView txtOrganizationInfo;
		
		public EditText txtMerchantCategory;
		public EditText txtProjectType;
		public EditText txtSupplier;
		
}
