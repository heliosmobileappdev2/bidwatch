package ph.helios.bidwatch.utilities;

import java.util.List;

import ph.helios.bidwatch.R;
import ph.helios.bidwatch.Model.Model_Procurement_List;
import ph.helios.bidwatch.listener.OnInterfaceClickListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class CustomDialog {

	public Dialog dialog;
	EditText etGroceryListName;
	Button dialog_btn_cancel;
	Button dialog_btn_add;
	Button dialog_btn_yes;
	Button dialog_btn_no;
	ImageButton imgbtn_closeads;
	ImageButton imgbtn_ads;
	TextView txtRemoveTitle;
	
	public void groceryListAddDialog(Context context, OnInterfaceClickListener callback, String val1, List<?> models,int... params) {
		dialog = new Dialog(context,R.style.Theme_Dialog);
		dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		switch (params[0]){
	
			case 1:
				dialog.setContentView(R.layout.dialog_procurementdetails);
				dialog.show();
				
				txtRemoveTitle  =  (TextView) dialog.findViewById(R.id.txtRemoveTitle);
				dialog_btn_yes 	   =  (Button) dialog.findViewById(R.id.btnDialogYes);
				ImageButton imgbtn_close	   =  (ImageButton) dialog.findViewById(R.id.imgbtn_close);

				txtRemoveTitle.setText("Remove " + val1 +"?");
				Object instructions = models.get(0);
				
				txtRemoveTitle.setText(Html.fromHtml("<b>Title:</b> " + ((Model_Procurement_List) instructions).getTender_title() + "<br /><br />" +
									   "<b>Category: </b>" +  ((Model_Procurement_List) instructions).getBusinessCategory() + "<br /><br />"+
									   "<b>Approved Budget: </b>" +  ((Model_Procurement_List) instructions).getApproved_budget() + "<br /><br />"+
									   "<b>Classification: </b>" +  ((Model_Procurement_List) instructions).getClassification() + "<br /><br />"+
									   "<b>Closing Date: </b>" +  ((Model_Procurement_List) instructions).getClosing_date() + "<br /><br />"+
									   "<b>Description: </b>" +  ((Model_Procurement_List) instructions).getDescription() + "<br /><br />"));
				imgbtn_close.setOnClickListener(new View.OnClickListener() {	
					@Override
					public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
				});	
			break;
		
		}
		dialog.setOnDismissListener(new OnDismissListener() {
		    public void onDismiss(final DialogInterface dialog) {
		    }
		});
	}
	
	public void dismissDialog(){
		dialog.dismiss();
	}
	
	
}
